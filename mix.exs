defmodule AlbertoElixirClient.MixProject do
  use Mix.Project

  def project do
    [
      app: :alberto_elixir_client,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "ix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :amqp, :ecto, :poison, :etcdc]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:poison, ">= 0.0.0"},
      {:ecto, "~> 2.2.10"},
      {:amqp, "~> 1.0", override: true},
      {:etcdc, github: "haroldvera/etcdc"}
    ]
  end
end
