defmodule Alberto.User do
   use Alberto.Schema
    @derive {
      Poison.Encoder,
      only: [
        :username, :password
      ]
    }
    schema "user" do
      field :username, :string
      field :password, :string
    end
    def changeset(model, params \\ %{}) do
      model
      |> Ecto.Changeset.cast(params, [:username, :password])
      |> Ecto.Changeset.validate_required(
           [
             :username, :password
           ]
         )
    end
  end
