defmodule Alberto.Nodo do
  use Alberto.Schema
  @derive {
    Poison.Encoder,
    only: [
      :unique_id, :tenant, :name, :create, :modifier, :alter_privileged,
      :read_privileged, :parent, :secondary_parent, :pathfs, :data, :type,
      :inserted_at, :updated_at, :title, :description, :content, :nodetype, :opts
    ]
  }
  schema "node" do
    field :unique_id, :string #identificador unico del nodo
    field :tenant, :string #nombre del tenant
    field :name, :string # name del nodo
    field :path, :string
    field :creator, :string # identificador del creador del nodo
    field :modifier, :string # identificador del creador del nodo
    field :alter_privileged, {:array, :string} # privilegios para modificar el nodo
    field :read_privileged, {:array, :string} # privilegios para read del nodo
    field :parent, :string #identificador del padre del nodo
    field :secondary_parent, {:array, :string} #identificadores de los padres secundarios de este nodo
    field :pathfs, :string #camina en el fs si es que fuese record con contenido
    field :data, :map # data libre del nodo, suele usarse para guardar metadata
    field :type, :string # type de negocio del nodo
    field :title, :string # titulo del nodo
    field :description, :string # breve descripcion del nodo
    field :content, :boolean # si este es nodo de contenido, tiene un documento en el fs
    field :nodetype, :string # nodetype soportados package | record; los record no contienen mas hijos
    field :inserted_at, :utc_datetime
    field :updated_at, :utc_datetime
    field :opts, :map

  end

  def changeset(node, params \\ %{}) do
    node
    |> Ecto.Changeset.cast(params, [:data, :updated_at, :alter_privileged, :read_privileged,
      :modifier, :secondary_parent, :title, :description, :opts, :path])
    |> Ecto.Changeset.validate_required(
         [
           :unique_id,
           :name,
           :modifier,
           :alter_privileged,
           :read_privileged,
           :parent,
           :data,
           :type,
           :inserted_at,
           :updated_at,
           :title,
           :content,
           :nodetype, :inserted_at, :updated_at
         ]
       )
  end
end
