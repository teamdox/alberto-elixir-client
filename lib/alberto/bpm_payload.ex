defmodule Alberto.BpmPayload do
  use Alberto.Schema
  @derive {
    Poison.Encoder,
    only: [
      :process_id, :namespace, :name, :contentref, :context
    ]
  }
  schema "bpmpayload" do
    field :process_id, :integer
    field :namespace, :string
    field :name, :string
    field :contentref, :string
    field :context, :map
  end
  def changeset(model, params \\ %{}) do
    model
    |> Ecto.Changeset.cast(params, [:context])
    |> Ecto.Changeset.validate_required(
         [
           :process_id, :namespace, :name, :contentref, :context
         ]
       )
  end
end
