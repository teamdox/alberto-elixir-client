defmodule Alberto do
  @etcd_key "/hosts"
  use AMQP
  alias Alberto.{User, BpmPayload}

  require Logger
  def conn(service) do
    :pg2.get_closest_pid(service)
  end

  def login(%User{username: username, password: password}, conn) do
    GenServer.call(conn, {:login, username, password})
  end
  def ping(conn), do: GenServer.call(conn, {:ping})
  def userinfo(ticket, conn), do: GenServer.call(conn, {:ticketinfo, ticket})

  def impuesto_verde(
        %{
          :data => %{
            "parser" => %{
              "documento" => %{
                "monto_neto" => montoneto
              },
              "vehiculo" => %{"cit" => cit}
            }
          }
        }, conn
      ) do
    GenServer.call(conn, {:valor_iv, cit, montoneto})
  end
  def merge(conn, nodo) do
    Logger.info "merge #{inspect nodo}"
    conn
    |> GenServer.call({:datamerge, nodo})
  end
  def merge(conn, unique_id, nodo) do
    conn
    |> GenServer.call({:datamerge, unique_id, nodo})
  end
  def get(conn, %{:unique_id => unique_id}) do
    conn
    |> GenServer.call({:node, unique_id})
  end
  def get(conn, unique_id) do
    conn
    |> GenServer.call({:node, unique_id})
  end

  def childs(conn, unique_id, :secondary) do
    conn
    |> GenServer.call({:nodechild, unique_id, :secondary})
  end

  def childs(conn, unique_id) do
    conn
    |> GenServer.call({:nodechild, unique_id})
  end

  def tenant(conn, tenantid) do
    conn
    |> GenServer.call({:tenant, tenantid})
  end

  def workflownext(user, %BpmPayload{namespace: namespace}) do
    {conn, _, taskid} = task_id_info(namespace)
    conn
    |> :pg2.get_closest_pid
    |> GenServer.call(
         {
           :endtask,
           taskid
           |> String.to_integer(),
           DateTime.utc_now,
           0,
           user["data"]["username"]
         }
       )
  end

  def world do
    :etcdc.get(@etcd_key)
    |> ok_tuple
    |> etcdc_nodes
    |> Enum.map(
         &(
           &1
           |> Map.get(:value)
           |> String.to_atom
           |> Node.connect)
       )
  end
  def ok_tuple({:ok, b}), do: b
  def etcdc_nodes(
        %{
          :node => %{
            :nodes => nodes
          }
        }
      ), do: nodes

  def amqp_params(), do: "amqp://#{System.get_env("AMQP_USERNAME")}:#{
    System.get_env("AMQP_PASSWORD")
  }@#{System.get_env("AMQP_HOST")}:#{
    System.get_env("AMQP_PORT")
  }"

  def amqp_params(:spie), do:  "amqp://#{System.get_env("AMQP_RECV_SPIE_USERNAME")}:#{
    System.get_env("AMQP_RECV_SPIE_PASSWORD")
  }@#{System.get_env("AMQP_RECV_SPIE_HOST")}:#{
    System.get_env("AMQP_RECV_SPIE_PORT")
  }"

  def amqp_bind(channel, exchange, queue) do
    channel
    |> Queue.declare(queue, durable: true)
    channel
    |> Exchange.fanout(exchange, durable: true)
    channel
    |> Queue.bind(exchange, queue)
    channel
  end

  defp task_id_info(p) do
    [_, service, task, process_id] = ~r/(.+):\/\/(.+)\/(\d+)/
                                     |> Regex.run(p)
    {service, task, process_id}
  end
  def node_children(conn, nodeid) do
    conn
    |> GenServer.call({:nodechild, nodeid, :secondary})
  end
  def userinfo_test, do:
    %User{username: "system", password: "changeme"}
    |> Alberto.login(
         :userservice
         |> Alberto.conn()
       )
    |> Alberto.userinfo(
         :userservice
         |> Alberto.conn()
       )
  def create_package(conn, type, tenant, data) do
    conn |> GenServer.call({:package, data, tenant, type})
  end
  def amqp_send(conn, queuename, msg) do
    Logger.info("sending #{inspect msg} to #{queuename}")
    #where
    ch_ex = System.get_env(queuename)
    conn
    |> Connection.open
    |> Alberto.ok_tuple
    |> Channel.open
    |> Alberto.ok_tuple
    |> amqp_bind(ch_ex, ch_ex)
    |> AMQP.Basic.publish(
         ch_ex,
         "",
         msg |> Poison.encode |> Alberto.ok_tuple()
       )
    msg
  end

  def alberto_user(), do:
    %User{username: System.get_env("ALBERTO_USERNAME"), password: System.get_env("ALBERTO_PASSWORD")}
    |> login(:userservice |> conn())
    |> userinfo(:userservice |> conn())

  def alberto_write_content(
        {filename, content}, tenant, type, description, data, user
      ), do:
        conn("node")
        |> GenServer.call(
             {
               :node,
               user,
               conn("node") |> GenServer.call({:package, tenant, type}),
               data,
               {filename, content},
               tenant,
               type,
               description
             }
           )

end
