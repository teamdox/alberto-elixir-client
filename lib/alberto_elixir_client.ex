defmodule AlbertoElixirClient do
  use Application
  require Logger
  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    children = [
      supervisor(Registry, [:unique, :msg]),
    ]
    opts = [strategy: :one_for_one, name: __MODULE__.Supervisor]
    Supervisor.start_link(children, opts)

  end
end
